<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>upload</title>
</head>

<body>

	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data">
		<label for="file">Filename:</label>
		<input type="file" name="file" id="file"><br>
		<input type="submit" name="submit" value="Submit">
	</form>
<a href="main.php">Back to home page</a>
	<hr>

	<?php
	echo "Session: " . $_SESSION["username"];
	$error=$_FILES["file"]["error"];
	$name=$_FILES["file"]["name"];
	$type=$_FILES["file"]["type"];
	$size=$_FILES["file"]["size"];
	$tname=$_FILES["file"]["tmp_name"];
	if ($error > 0)
	{
		echo "Error: " . $error . "<br>";
	}
	else if(isset($_POST['submit']))
	{
		echo "Upload: " . $name . "<br>";
		echo "Type: " . $type . "<br>";
		echo "Size: " . ( $size/ 1024) . " kB<br>";
		echo "Stored in: " . $tname . "<br>";

//move the file
		$dir="../security/" . $_SESSION["username"] . "/";
		if (file_exists($dir . $name)) {
			echo $name . " already exists.";
		}
		else{
			move_uploaded_file($tname, $dir . $name);
			echo "Now stored in: " . $dir . $name;
		}

	}
	?>

</body>
</html>
