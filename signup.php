<?php
ob_start();
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Sign up</title>
	<style type="text/css">
	.error {color: #FF0000;}
	</style>
</head>
<body>

	<?php
	//session_destroy();
	$redirect_page = "main.php";
	$redirect=false;
	
	$username=$tmpusername="";
	$unErr=$pwdErr=$cpwdErr="";
	$name_exist=$dir_result=false;

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		if(empty($_POST["username"]))
		{
			$unErr="Please enter your username.";
		}
		else
		{
			$tmpusername=test_input($_POST["username"]);
			if (!preg_match("/^\w+$/", $_POST["username"])) 
			{
				$unErr= "Only letters, numbers, and underline allowed";
			}
			else
			{
				$username=test_input($_POST["username"]);
			}

			$name_exist=check_exist($username);
			if($name_exist)
				$unErr="user already exist";

			if(isset($_POST['submit']) && $username!=NULL && $name_exist==false){
				//create dir to store users' file
				$_SESSION["username"]=$username;
				$path="../security/" . $username;
				if(!file_exists($path)){
					$dir_result=mkdir($path,0755,true);
					chown($path, apache);
				}else{
					echo "folder is already exit";
				}
				//redirect to home page after signing up
				if ($dir_result) {
					$file=fopen("../security/users.txt","a+") or exit("Unable to open file!");
					$name=$username . "\n";
					fwrite($file, $name);
					fclose($file);
					$_SESSION["username"]=$username;
					$redirect=true;
				}
				//
				if ($redirect==true) {
					header("Location: ".$redirect_page);
				}

			}
		}

		echo "Session:" . $_SESSION["username"];
	}

	function check_exist($data){
		$userfile=fopen("../security/users.txt", "r");
		$srt="";
		$check=false;
		
		while (!feof($userfile)) {
			$str=trim(fgets($userfile));
			if(($str==$data) && ($data != "")){
				$check=true;
				break;
			}
		}
		return $check;
	}

	function test_input($data){
		$data =trim($data);
		$data = stripcslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	?>

	<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		Username: <input type="text" name="username" value="<?php echo $tmpusername;?>">
		<span class="error">*<?php echo $unErr;?></span>
		<br>
		<button type="submit" name="submit" value="submit">Sign Up</button>
	</form>






</body>
</html>