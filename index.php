<?php
ob_start();
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>File Sharing System Log In</title>
	<style type="text/css">
	.error {color: #FF0000;}
	</style>
</head>
<body>

	<?php
	$redirect_page = "main.php";
	$redirect=false;
	
	$username=$tmpusername="";
	$unErr="";
	$name_exist=false;

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		if(isset($_POST["signup"])){
			header("Location: signup.php");
		}

		if(empty($_POST["username"]))
		{
			$unErr="Please enter your username.";
			session_destroy();
		}
		else
		{
			$tmpusername=test_input($_POST["username"]);
			if (!preg_match("/^\w+$/", $_POST["username"])) 
			{
				$unErr= "Only letters, numbers, and underline allowed";
			}
			else
			{
				$username=test_input($_POST["username"]);
				$name_exist=check_exist($username);
				echo "<h1>用户是否存在".$name_exist."</h1>";

				if(!$name_exist){
					$unErr="username does not exist.";
					session_destroy();
				}
				else if(isset($_POST['submit']) && $username!=NULL){
					$_SESSION["username"]=$username;
					//redirect to home page after signing up
					header("Location: ".$redirect_page);
				}

				//Sign Up
				
			}
		}
	}

	function check_exist($data){
		$userfile=fopen("../security/users.txt", "r")or exit("Unable to open file!");
		$srt="";
		$check=false;
		
		while (!feof($userfile)) {
			$str=trim(fgets($userfile));
			if(($str==$data) && ($data != "")){
				$check=true;
				break;
			}
		}
		return $check;
	}


	function test_input($data){
		$data =trim($data);
		$data = stripcslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	?>

	<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		Username: <input type="text" name="username" value="<?php echo $tmpusername;?>">
		<span class="error">*<?php echo $unErr;?></span>
		<br>
		<button type="submit" name="submit" value="submit">Log In</button>
		<br>
		<button type="submit" name="signup" value="signup">Sign Up</button>
	</form>
	
</body>
</html>